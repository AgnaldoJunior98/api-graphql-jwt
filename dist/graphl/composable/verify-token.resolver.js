"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("./../../utils/utils");
const jwt = require("jsonwebtoken");
exports.verifyTokenResolver = (resolver) => {
    return (parent, args, context, info) => {
        const token = context.authorization ? context.authorization.split(' ')[1] : undefined;
        jwt.verify(token, utils_1.JWT_SECRET, (err, decoded) => {
            if (!err) {
                return resolver(parent, args, context, info);
            }
            throw new Error(`${err.name}:${err.message}`);
        });
    };
};
